<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>--Busca--</title>
    <meta charset="utf-8" />
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.js" ></script>
	
    
   
   		<script language="JavaScript" type="text/javascript">
		function controleCampo(e){
			if (e == 1) {
				document.getElementById("titulo").style.display = 'block';
				document.getElementById("datas").style.display = "none";
				document.getElementById("btitulo").disabled = false;
				document.getElementById("dinicio").disabled = true;
				document.getElementById("dfim").disabled = true;
				
				
			}else if(e == 2){				
				document.getElementById("datas").style.display = "block";
				document.getElementById("dinicio").disabled = false;
				document.getElementById("dfim").disabled = false;
				document.getElementById("btitulo").disabled = true;
				document.getElementById("titulo").style.display = "none";
			}else{
				document.getElementById("btitulo").disabled = true;
				document.getElementById("dinicio").disabled = true;
				document.getElementById("dfim").disabled = true;
				document.getElementById("titulo").style.display = "none";
				document.getElementById("datas").style.display = "none";
			}
		}

   </script>
 
</head>
<body>
    <form name="meu_form" method="post" action="index.php">
		<h1>Pesquisa</h1>
		<p>
			<input type="radio" id="Busca" name="Busca" value="ALL" onclick="controleCampo(0)" checked /> <label for="Busca">Buscar todas as notícias</label>			
			<input type="radio" id="Busca" name="Busca" value="TITULO" onclick="controleCampo(1)" /> <label for="Busca">Filtrar notícias por palavra-chave no título</label>
			<input type="radio" id="Busca" name="Busca" value="DATA" onclick="controleCampo(2)" /> <label for="Busca">Filtrar notícias por data (ínicio - fim)</label>
			<input type="radio" id="Busca" name="Busca" value="CONTA" onclick="controleCampo(3)" /> <label for="Busca">Contar quantas notícias tiveram na última hora</label>
		<div id="titulo" style="display:none">
			<p>
				<input type="text" id="btitulo" name="btitulo"><label for="btitulo">TITULO</label>
			</p>
		</div>
		<div id="datas" style="display:none">
			<p>
				<input type="date" id="dinicio" name="dinicio"><label for="dinicio">Data Inicio</label>
				<input type="date" id="dfim" name="dfim"><label for="dfim">Data Fim</label>
			</p>
		</div>
		
		<p>
			<label for="TipoRetorno">Retornar XML</label> <input type="radio" id="TipoRetorno" name="TipoRetorno" value="ReturnXML" checked>
			<label for="TipoRetorno">Retornar JSON</label> <input type="radio" id="TipoRetorno" name="TipoRetorno" value="ReturnJSON">
		</p>
        
        <p class="submit">
            <input type="submit" onclick="Enviar();" value="Enviar" />
        </p>
 
    </form>
</body>
</html>