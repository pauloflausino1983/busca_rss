<?php

class RssFilter {
	public $returntype = 'xml';
	private $UltimaHora = 0;
	public $date = '';
	public $timestampNow = 0;
	public $timestamp = 0;
	public $words = array();
	public $tipobusca = '';
  
  function stristrArray($haystack, $needle) {
    if (!is_array($needle)) {
      $needle = [$needle];
    }
    foreach ($needle as $searchstring) {
      $found = stristr($haystack, $searchstring);
      if ($found) {
        return $found;
      }
    }
    return false;
  }

  function getAttribute($string, $attribute = '') {
    if ($string->length === 0) {
      $result = '';
    } else {
      if ($attribute) {
        $result = $string->item(0)->getAttribute($attribute);
      } else {
        if ($string->item(0)->childNodes->item(1)) {
          $result = $string->item(0)->childNodes->item(1)->nodeValue;
        } elseif ($string->item(0)->childNodes->item(0)) {
          $result = $string->item(0)->childNodes->item(0)->nodeValue;
        } else {
          $result = '';
        }
      }
    }
    return $result;
  }

  function init($source) {
    

    $xmlDoc = new DOMDocument();
    $source = trim(file_get_contents($source));
    $xmlDoc->loadXML($source);

    if ($xmlDoc->getElementsByTagName('feed')->length) {
      $type = 'atom';
    } elseif ($xmlDoc->getElementsByTagName('rss')->length) {
      $type = 'rss';
    } else {
      $type = '';
    }
	
	
    if ($type === 'atom') {
      $rssTitle = $this->getAttribute($xmlDoc->getElementsByTagName('title'));
      $rssLink = $this->getAttribute($xmlDoc->getElementsByTagName('link'), 'href');
      $rssDescription = '';
      $items = $xmlDoc->getElementsByTagName('entry');
      $countItems = ($xmlDoc->getElementsByTagName('entry')->length);
      $combineItems = '';
	 
      for ($i = 0; $i < $countItems; $i++) {
        $item = $items->item($i);
        $title = htmlspecialchars($this->getAttribute($item->getElementsByTagName('title')));
        $link = htmlspecialchars($this->getAttribute($item->getElementsByTagName('link'), 'href'));
        $description = htmlspecialchars($this->getAttribute($item->getElementsByTagName('content')));
        $pubDate = $this->getAttribute($item->getElementsByTagName('published'));
        $updated = $this->getAttribute($item->getElementsByTagName('updated'));		
        $published = $pubDate ? $pubDate : $updated;
		$timePub = strtotime($published);
		if($timePub >= $this->timestamp && $timePub <= $this->$timestampNow){
			$this->UltimaHora++;
		}
		
		if($this->tipobusca == 'TITULO'){
			if(!empty($this->words)){
				if ($this->stristrArray($title, $this->words)) {
				  $combineItems .= '<item>
					<title>' . $title . '</title>
					<link>' . $link . '</link>
					<description>' . $description . '</description>
					<pubDate>' . $published . '</pubDate>
				  </item>';
				}
			}else{
				$combineItems .= '<item>
					<title>' . $title . '</title>
					<link>' . $link . '</link>
					<description>' . $description . '</description>
					<pubDate>' . $published . '</pubDate>
				  </item>';				
			}
		}
		
		if($this->tipobusca == 'DATA'){
			if($timePub >= $this->timestamp && $timePub <= $this->$timestampNow){
				$combineItems .= '<item>
					<title>' . $title . '</title>
					<link>' . $link . '</link>
					<description>' . $description . '</description>
					<pubDate>' . $published . '</pubDate>
				  </item>';	
			}
			
		}
      }
    } elseif ($type === 'rss') {
      $channel = $xmlDoc->getElementsByTagName('channel')->item(0);
      $rssTitle = $this->getAttribute($channel->getElementsByTagName('title'));
      $rssLink = $this->getAttribute($channel->getElementsByTagName('link'));
      $rssDescription = $this->getAttribute($channel->getElementsByTagName('description'));
      $items = $xmlDoc->getElementsByTagName('item');
      $countItems = ($xmlDoc->getElementsByTagName('item')->length);
      $combineItems = '';
	 
      for ($i = 0; $i < $countItems; $i++) {
        $item = $items->item($i);
        $title = htmlspecialchars($this->getAttribute($item->getElementsByTagName('title')));
        $link = htmlspecialchars($this->getAttribute($item->getElementsByTagName('link')));
        $description1 = htmlspecialchars($this->getAttribute($item->getElementsByTagName('description')));
        $description2 = htmlspecialchars($this->getAttribute($item->getElementsByTagName('encoded')));
        $description = $description1 ? $description1 : $description2;
        $pubDate = $this->getAttribute($item->getElementsByTagName('pubDate'));
		$timePub = strtotime($pubDate);
		if($timePub >= $this->timestamp && $timePub <= $this->timestampNow){
			$this->UltimaHora++;
		}
		
		if($this->tipobusca == 'ALL'){
			$combineItems .= '<item>
					<title>' . $title . '</title>
					<link>' . $link . '</link>
					<description>' . $description . '</description>
					<pubDate>' . $pubDate . '</pubDate>
				  </item>';
		}
		
		if($this->tipobusca == 'TITULO'){
			if(!empty($this->words)){
				if ($this->stristrArray($title, $this->words)) {
				  $combineItems .= '<item>
					<title>' . $title . '</title>
					<link>' . $link . '</link>
					<description>' . $description . '</description>
					<pubDate>' . $pubDate . '</pubDate>
				  </item>';
				}
			}else{
				$combineItems .= '<item>
					<title>' . $title . '</title>
					<link>' . $link . '</link>
					<description>' . $description . '</description>
					<pubDate>' . $pubDate . '</pubDate>
				  </item>';				
			}
		}
		
		if($this->tipobusca == 'DATA'){
			if($timePub < $this->timestamp  && $timePub > $this->timestampNow){
				$combineItems .= '<item>
					<title>' . $title . '</title>
					<link>' . $link . '</link>
					<description>' . $description . '</description>
					<pubDate>' . $pubDate . '</pubDate>
				  </item>';	
			}
			
		}
      }
    }

    if ($this->returntype == 'xml') {
      $result = '<?xml version="1.0" encoding="UTF-8"?>
      <rss version="2.0">
      <channel>
        <title>' . htmlspecialchars($rssTitle) . '</title>
        <link>' . htmlspecialchars($rssLink) . '</link>
        <description>' . htmlspecialchars($rssDescription) . '</description>
        ' . $combineItems . '
      </channel>
      </rss>';
    }else if($this->returntype == 'json'){
		$xmlresult = '<?xml version="1.0" encoding="UTF-8"?>
		<rss version="2.0">
			<channel>
				<title>' . htmlspecialchars($rssTitle) . '</title>
				<link>' . htmlspecialchars($rssLink) . '</link>
				<description>' . htmlspecialchars($rssDescription) . '</description>
				' . $combineItems . '
			</channel>
		</rss>';
		
		$simpleXml = simplexml_load_string($xmlresult);
		$result = json_encode($simpleXml);
		
	  
	}elseif($this->returntype == 'ultimahora'){
		$result = $this->UltimaHora;
	} else {
      $result = 'undefined type';
    }

    return $result;
  }
}
